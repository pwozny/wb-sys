


# Python module for Direct-Sequence Spectrum Signals 

## Installation

### Linux
- locally `python3 setup.py develop --user` 
- globally `sudo python3 setup.py develop`

### Mac
- TODO

### Windows
- TODO

## Requirements for project
- simulating direct-sequence spread spectrum transmission 
  - sending random data
  - sending test patterns
- TOTHINK: Hardware verifiaction
-      

## Additional resources/references

- https://luka.strizic.info/post/Direct-Sequence-Spread-Spectrum-Illustrated/
- http://www.wirelesscommunication.nl/reference/chaptr05/cdma/codes/gold.html
- http://www.partow.net/programming/polynomials/index.html
- https://www.maximintegrated.com/en/design/technical-documents/tutorials/1/1890.html


## Development dependencies
- https://docs.python.org/3/library/doctest.html#module-doctest
- https://docs.python.org/3/library/unittest.html#module-unittest
- 





