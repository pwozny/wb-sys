#!/usr/bin/env python3

import numpy as np
import scipy.signal as sp
import matplotlib.pyplot as plt
from bitstring import BitArray
from pylfsr import LFSR


def tobits(s):
    """
    https://github.com/lstrz/dsss-illustrated/blob/2b2cda5cbe0942f5cc5c4472dcb72a37d6e2033a/main.py#L16
    """
    result = []
    for c in s:
        bits = bin(ord(c))[2:]
        bits = '00000000'[len(bits):] + bits
        result.extend([int(b) for b in bits][1:])
    return result


def frombits(bits):
    chars = []
    for b in range(int(len(bits) / 7)):
        byte = bits[b * 7:(b + 1) * 7]
        chars.append(chr(int(''.join([str(bit) for bit in byte]), 2)))
    return ''.join(chars)


class Dsss:
    """
    Direct Sequence Spectrum Signal
    Commands with leading'##' are left on pourpuse to
    ease debug during development
    """

    def __init__(self, debug=False):
        """
        select first 5-bit feedback polynomial from
        known list of primitive irreducible polynomials
        TODO: fix Initial state of LFSR register
        """
        print("\n DSSS module")
        # DEBUG:
        # L = LFSR()
        # _POLY_LIST = L.get_fpolyList(m=4)
        # _POLY = _POLY_LIST[0]
        # self.rng = LFSR(fpoly=_POLY, verbose=False)
        # Pseudorandom Spread Sequence
        # of type List <class 'numpy.int64'>
        # self.pss = self.rng.runKCycle(11)  # runFullCycle()
        # self.ss_one = BitArray(self.pss)
        # self.ss_zero = BitArray(self.pss)
        # self.ss_zero.invert()
        #
        # 11-bit Barker sequence
        self.ss_one = np.array(
            [1, 1, 1, -1, -1, -1, 1, -1, -1, 1, -1], dtype=int)
        self.ss_zero = np.negative(self.ss_one)

        self.ss_len = len(self.ss_one)

        self.db = debug
        if self.db:
            print("Spread sequence for 1: ", self.ss_one)
            print("Spread sequence for 0: ", self.ss_zero)

    def tx(self, input_string):

        np_input_bytes = np.array(tobits(input_string))
        spread_data = np.empty((1, 1), dtype=np.uint8)
        self.nb = len(np_input_bytes)

        for i in np_input_bytes:
            if i == 1:
                spread_data = np.append(spread_data, self.ss_one)
            else:
                spread_data = np.append(spread_data, self.ss_zero)
        spread_data = spread_data[1:]

        # Generating modulation parameters

        message_frequency = 1e3
        carrier_frequency = 2e3
        samples_per_carrier_period = 10

        time_start = 0.0
        time_end = 1.0 / message_frequency * len(spread_data)
        time_duration = time_end - time_start
        dt = 1 / carrier_frequency / samples_per_carrier_period
        time_steps = time_duration / dt
        self.t = np.linspace(time_start, time_end, round(time_steps))
        self.carrier = 1 * np.sin(2 * np.pi * carrier_frequency * self.t)

        # modulate signal
        data_out = np.repeat(spread_data, len(
            self.carrier)/len(spread_data)) * self.carrier

        if self.db:
            print("Input string: ", input_string)
            print("Binary representation: ", np_input_bytes)

            x_input_data = np.arange(0, time_steps)
            y_input_data = np.repeat(
                np_input_bytes, self.ss_len*samples_per_carrier_period*carrier_frequency/message_frequency)
            x_chip_data = x_input_data
            y_chip_data = np.repeat(
                spread_data, samples_per_carrier_period*carrier_frequency/message_frequency)
            x_carrier = x_input_data
            y_carrier = self.carrier
            x_modulated = x_input_data
            y_modulated = data_out

            fig, axs = plt.subplots(5, 1)
            axs[0].set_title("Bits to send")
            axs[0].step(x_input_data, y_input_data)
            axs[1].set_title("Spread (chirped) sequence")
            axs[1].step(x_chip_data, y_chip_data)
            axs[2].set_title("Carrier")
            axs[2].plot(x_carrier, y_carrier)
            axs[3].set_title("Modulated Data")
            axs[3].plot(x_modulated, y_modulated)
            axs[4].set_title("abs real FFT of Modulated Data")
            axs[4].plot(abs(np.fft.rfft(y_modulated)))
            plt.tight_layout()
            # plt.show()

        return data_out

    def simulate_ether(self, input_data, noise_level: float = 0.5):
        noise_offset = 0
        output_signal = input_data + \
            np.random.normal(noise_offset, noise_level, len(input_data))
        if self.db:
            fig, axs = plt.subplots(2, 2)
            axs[0, 0].set_title("Data without noise")
            axs[0, 0].plot(input_data)
            axs[0, 1].set_title("Data without noise -")
            axs[0, 1].plot(abs(np.fft.rfft(input_data)))
            axs[1, 0].set_title("Data with noise")
            axs[1, 0].plot(output_signal)
            axs[1, 1].set_title("Data with noise - FFT")
            axs[1, 1].plot(abs(np.fft.rfft(output_signal)))
            plt.tight_layout()
            # plt.show()

        return output_signal

    def decode(self, input_data):
        # Turns 0 1 0 1 into digits[0] digits[1] digits[0] digits[1]
        def binarize_dat(xs, digits):
            return np.array([digits[0] if x <= 0 else digits[1] for x in xs])


        # plt.close('all')

        demod = self.carrier * input_data

        sos = sp.butter(30, 1500, 'lp', fs=20e3, output='sos')
        filtered = sp.sosfilt(sos, demod)

        # binarize filtered demodulated signal
        binarized = binarize_dat(filtered, [0, 1])



        # if self.db:
        fig, axs_demod = plt.subplots(2, 2)
        axs_demod[0, 0].set_title("Demod data before filt")
        axs_demod[0, 0].plot(demod)
        axs_demod[0, 1].set_title("Demod data before filt - FFT")
        axs_demod[0, 1].plot(abs(np.fft.rfft(demod)))
        axs_demod[1, 0].set_title("Demod data after filt")
        axs_demod[1, 0].plot(filtered)
        axs_demod[1, 1].set_title("Demod data after filt - FFT")
        axs_demod[1, 1].plot(abs(np.fft.rfft(filtered)))

        fig, axs_bin = plt.subplots(1, 1)
        axs_bin.set_title("Binarized data")
        axs_bin.plot(binarized)

        binarized = np.roll(binarized, -41)


        out_data = []
        nom_of_chunks = 7
        ss_zero_long = np.repeat(
            binarize_dat(self.ss_zero, [0, 1]), 20)
        ss_one_long = np.repeat(
            binarize_dat(self.ss_one, [0, 1]), 20)

        fig, axs = plt.subplots(nom_of_chunks, 3)
        for i  in range(nom_of_chunks):
            chunks = np.split(binarized, nom_of_chunks)
            zero_mul = ss_zero_long * chunks[i]
            one_mul =  ss_one_long * chunks[i]

            out_data.append([1 if np.sum(one_mul)> np.sum(zero_mul) else 0 ])

            axs[i, 0].set_title("chunk")
            axs[i, 0].plot(chunks[i])
            axs[i, 1].set_title("chunk * zero chirp")
            axs[i, 1].plot(zero_mul)
            axs[i, 2].set_title("chunk * one chirp")
            axs[i, 2].plot(one_mul)
            plt.subplots_adjust(hspace=0.06)

        if self.db:
            plt.tight_layout()
            plt.show()

        out_data=  np.ravel(out_data)

        print("receieved data: ",  out_data)








        # axs[2, 1].set_title("resized one chirp")
        # axs[2, 1].plot(resized_chirp_one)

        # fig2, axs2 = plt.subplots(3, 1)
        # axs2[0].set_title("Despreaded 1 data")
        # axs2[0].plot(despreaded_one)
        # axs2[1].set_title("Despreaded 0 data")
        # axs2[1].plot(despreaded_zero)
        # axs2[1].set_title("Despreaded data func")


        # test_conv = np.repeat(
        #     binarize_dat(self.ss_zero, [0, 1]), 20)
        # test_conv2 = []
        # for chunk in np.split(binarized, 7):

        #     data_conv = sp.convolve(chunk,test_conv)
        #     test_conv2.append(data_conv)

        # print(len(test_conv2))

        # fig_tst, axs3 = plt.subplots(len(test_conv2), 2)
        # for i  in range(len(test_conv2)):
        #     axs3[i, 0].set_title("chunk ")
        #     axs3[i, 0].plot(chunk)
        #     axs3[i, 1].set_title("Despreaded ")
        #     axs3[i, 1].plot(test_conv2[i])




        return demod


if __name__ == '__main__':
    _STRING_TO_ENCODE = "X"
    dsss = Dsss(debug=True)
    modulated_data = dsss.tx(_STRING_TO_ENCODE)
    distorted_data = dsss.simulate_ether(modulated_data)
    demodulated_data = dsss.decode(distorted_data)

    # _BYTE_TO_ENCODE = BitArray(hex="0xFA")

    # encoded = dsss.encode(_BYTE_TO_ENCODE)
    # dsss.modulate(input_data=encoded)
    # dsss.decode(encoded)
