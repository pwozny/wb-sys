#!/usr/bin/env python3

from setuptools import setup

setup(name='pydsss',
      version='0.1',
      description='Python implementation of Direct Sequence Spread Spectrum',
      author='Pawel Wozny',
      author_email='pawel.wozny@protonmail.ch',
      url='https://gitlab.cern.ch/pwozny/wb-sys',
    #   packages=['pydsss'],
      install_requires=['numpy', 'scipy', 'matplotlib','pylfsr','bitstring', 'pytest'],
      )
