#!/usr/bin/env python3
"""
Test suite for DSSS methods

To run those test, cd into this directory and type
`pytest tests.py`

"""

from pydsss.pydsss import Dsss
from bitstring import BitArray


def test_encode_one_bit():
    _BYTE_TO_ENCODE = BitArray(bin="0")
    _EXPECTED_VALUE =  BitArray(hex="0x14dc")

    dsss = Dsss()
    result = dsss.encode(_BYTE_TO_ENCODE)
    assert _EXPECTED_VALUE == result


def test_encode_one_byte():
    _BYTE_TO_ENCODE = BitArray(hex="0xFA")
    _EXPECTED_VALUE =  BitArray(hex="eb22eb22eb22eb22eb2214dceb2214dc")
    
    dsss = Dsss()
    result = dsss.encode(_BYTE_TO_ENCODE)
    assert _EXPECTED_VALUE == result



# TODO: create rest of unit tests      
    # def test_encode_text_string(self)
    #     dsss = Dsss()

    # def test_decode_one_byte(self):
    #     dsss = Dsss()

    # def test_decode_text_string(self):
    #     dsss = Dsss()

    # def test_signal_path(self):
    #     dsss = Dsss()

    # def test_plots(self):
    #     dsss = Dsss()

if __name__ == '__main__':
    print("Execute tests by typing 'pytest' in git root dir.")
    exit(1)
